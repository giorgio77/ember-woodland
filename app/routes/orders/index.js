// app/routes/orders/index.js

import Ember from 'ember';

export default Ember.Route.extend({
	
	
	
	model() {
		const store = this.get('store');
		return store.newOrder();
	}, 
	
	actions: {
		createOrder(order) {
			this.get('store').saveOrder(order);
			this.transitionTo('orders.order', order);
		}, 
		
		addToItemQuantity(lineItem, amount) {
			lineItem.incrementProperty('quantity', amount);
		}
	}
});